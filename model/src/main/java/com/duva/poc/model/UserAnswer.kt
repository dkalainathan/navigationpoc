package com.duva.poc.model

import java.io.Serializable

data class UserAnswer(
    val correct: Boolean,
    val expected: String?
) : Serializable