package com.duva.poc.model

import java.io.Serializable

data class QuestionSequence(
    val done: List<Int>,
    val score: Int
) : Serializable