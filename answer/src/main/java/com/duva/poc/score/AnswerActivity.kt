package com.duva.poc.answer

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.duva.poc.answer.navigation.NavigatorProvider
import com.duva.poc.model.QuestionSequence
import com.duva.poc.model.UserAnswer
import kotlinx.android.synthetic.main.layout_answer.*
import java.lang.IllegalStateException

class AnswerActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_answer)

        setupViewFromArgs()
    }

    private fun setupViewFromArgs() {
        val userAnswer = intent.getSerializableExtra(ANSWER_KEY) as UserAnswer
        val sequence = intent.getSerializableExtra(QUESTION_SEQUENCE_KEY) as QuestionSequence

        val currentScore = sequence.score + (if (userAnswer.correct) 1 else 0)
        val newSequence = sequence.copy(score = currentScore)

        if (!userAnswer.correct) {
            good_answer.visibility = View.GONE
            bad_answer_group.visibility = View.VISIBLE
            correst_answer.text = userAnswer.expected
        }

        next_question.setOnClickListener {
            val done = sequence.done
            val nextQuestionIntent = when (sequence.done.size) {
                1 -> NavigatorProvider.navigator.getQuestionTwoIntent(this, newSequence)
                2 -> NavigatorProvider.navigator.getQuestionThreeIntent(this, newSequence)
                3 -> NavigatorProvider.navigator.getScoreIntent(this, newSequence)
                else -> throw IllegalStateException()
            }

            startActivity(nextQuestionIntent)
        }
    }

    companion object {
        private const val ANSWER_KEY = "AnswerActivity.ANSWER_KEY"
        private const val QUESTION_SEQUENCE_KEY = "AnswerActivity.QUESTION_SEQUENCE"

        fun getStartArguments(
            answer: UserAnswer,
            sequence: QuestionSequence
        ):Bundle {
            return Bundle().apply {
                putSerializable(ANSWER_KEY, answer)
                putSerializable(QUESTION_SEQUENCE_KEY, sequence)
            }
        }
    }
}