package com.duva.poc.answer.navigation

import android.content.Context
import android.content.Intent
import com.duva.poc.answer.AnswerActivity
import com.duva.poc.model.QuestionSequence
import com.duva.poc.model.UserAnswer

object AnswerIntents {

    fun anwserIntent(
        context: Context,
        answer: UserAnswer,
        sequence: QuestionSequence
    ): Intent {
        return Intent(context, AnswerActivity::class.java).apply {
            putExtras(AnswerActivity.getStartArguments(answer, sequence))
        }
    }
}