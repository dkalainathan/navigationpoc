package com.duva.poc.score

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.duva.poc.model.QuestionSequence
import com.duva.poc.score.navigation.NavigatorProvider
import com.duva.poc.score.navigation.ScoreIntents
import kotlinx.android.synthetic.main.layout_score.*


class ScoreActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_score)

        restart_button.setOnClickListener {
            val intent = NavigatorProvider.navigator.getHomeIntent(this)
            startActivity(intent)
        }

        setupViewFromArgs()
    }

    private fun setupViewFromArgs() {
        val sequence = intent.getSerializableExtra(ScoreIntents.QUESTION_SEQUENCE_KEY) as QuestionSequence
        final_score.text = "${sequence.score} / ${sequence.done.size}"
    }
}