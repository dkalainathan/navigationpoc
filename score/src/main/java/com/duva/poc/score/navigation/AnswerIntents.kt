package com.duva.poc.score.navigation

import android.content.Context
import android.content.Intent
import com.duva.poc.score.ScoreActivity
import com.duva.poc.model.QuestionSequence

object ScoreIntents {

    internal const val QUESTION_SEQUENCE_KEY = "Score.QUESTION_SEQUENCE"

    fun getScoreIntent(
        context: Context,
        sequence: QuestionSequence
    ): Intent {
        return Intent(context, ScoreActivity::class.java).apply {
            putExtra(QUESTION_SEQUENCE_KEY, sequence)
            addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
        }
    }
}