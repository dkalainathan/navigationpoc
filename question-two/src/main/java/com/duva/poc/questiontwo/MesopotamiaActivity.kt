package com.duva.poc.questiontwo

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.duva.poc.model.QuestionSequence
import com.duva.poc.model.UserAnswer
import com.duva.poc.questiontwo.navigation.NavigatorProvider
import com.duva.poc.questiontwo.navigation.QuestionTwoIntents
import kotlinx.android.synthetic.main.activity_mesopotamia.*

class MesopotamiaActivity: AppCompatActivity() {

    lateinit var sequence: QuestionSequence

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_mesopotamia)

        sequence = intent.getSerializableExtra(QuestionTwoIntents.SEQUENCE_KEY) as QuestionSequence

        nabuchodonosor.setOnClickListener { redirectBadAnswer() }
        iskandar.setOnClickListener { redirectBadAnswer() }
        ali_baba.setOnClickListener { redirectBadAnswer() }

        gilgamesh.setOnClickListener { redirectGoodAnswer() }
    }

    private fun redirectBadAnswer() {
        val intent = NavigatorProvider.navigator.getAnwserIntent(
            context = this,
            answer = UserAnswer(false, gilgamesh.text.toString()),
            sequence = sequence.copy(sequence.done + 2)
        )

        startActivity(intent)
    }

    private fun redirectGoodAnswer() {
        val intent = NavigatorProvider.navigator.getAnwserIntent(
            context = this,
            answer = UserAnswer(true, ""),
            sequence = sequence.copy(sequence.done + 2)
        )

        startActivity(intent)
    }
}