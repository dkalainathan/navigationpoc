package com.duva.poc.questiontwo.navigation

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.duva.poc.model.QuestionSequence
import com.duva.poc.questiontwo.MesopotamiaActivity

object QuestionTwoIntents {

    internal const val SEQUENCE_KEY = "QuestionTwo.SEQUENCE_KEY"

    fun questionTwoIntent(
        context: Context,
        sequence: QuestionSequence
    ): Intent {
        return Intent(context, MesopotamiaActivity::class.java).apply {
            putExtra(SEQUENCE_KEY, sequence)
        }
    }
}