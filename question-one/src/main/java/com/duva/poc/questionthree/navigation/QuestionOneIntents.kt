package com.duva.poc.question_one.navigation

import android.content.Context
import android.content.Intent
import com.duva.poc.model.QuestionSequence
import com.duva.poc.question_one.QuestionOne

object QuestionOneIntents {

    fun questionOneIntent(
        context: Context,
        sequence: QuestionSequence
    ): Intent {
        return Intent(context, QuestionOne::class.java).apply {
            putExtras(QuestionOne.getStartArguments(sequence))
        }
    }
}