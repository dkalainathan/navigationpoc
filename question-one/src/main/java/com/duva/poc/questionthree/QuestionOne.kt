package com.duva.poc.question_one

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.duva.poc.model.QuestionSequence
import com.duva.poc.model.UserAnswer
import com.duva.poc.question_one.navigation.NavigatorProvider
import kotlinx.android.synthetic.main.layout_question_one.*

class QuestionOne: AppCompatActivity() {

    lateinit var sequence: QuestionSequence

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_question_one)

        sequence = intent.getSerializableExtra(SEQUENCE_KEY) as QuestionSequence

        platon.setOnClickListener { redirectBadAnswer() }
        socrate.setOnClickListener { redirectBadAnswer() }
        archimede.setOnClickListener { redirectBadAnswer() }

        aristote.setOnClickListener { redirectGoodAnswer() }
    }

    private fun redirectBadAnswer() {
        val intent = NavigatorProvider.navigator.getAnwserIntent(
            context = this,
            answer = UserAnswer(false, "Aristote"),
            sequence = sequence.copy(sequence.done + 1)
        )

        startActivity(intent)
    }

    private fun redirectGoodAnswer() {
        val intent = NavigatorProvider.navigator.getAnwserIntent(
            context = this,
            answer = UserAnswer(true, ""),
            sequence = sequence.copy(sequence.done + 1)
        )

        startActivity(intent)
    }

    companion object {
        private const val SEQUENCE_KEY = "QuestionOne.SEQUENCE_KEY"

        fun getStartArguments(sequence: QuestionSequence): Bundle {
            return Bundle().apply {
                putSerializable(SEQUENCE_KEY, sequence)
            }
        }
    }
}