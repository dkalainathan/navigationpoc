package com.duva.poc.navigation

import android.content.Context
import android.content.Intent
import com.duva.poc.model.QuestionSequence
import com.duva.poc.model.UserAnswer

interface Navigator {

    fun getHomeIntent(context: Context): Intent

    fun getQuestionOneIntent(
        context: Context,
        sequence: QuestionSequence
    ): Intent

    fun getQuestionTwoIntent(
        context: Context,
        sequence: QuestionSequence
    ): Intent

    fun getQuestionThreeIntent(
        context: Context,
        sequence: QuestionSequence
    ): Intent

    fun getAnwserIntent(
        context: Context,
        answer: UserAnswer,
        sequence: QuestionSequence
    ): Intent

    fun getScoreIntent(
        context: Context,
        sequence: QuestionSequence
    ): Intent
}