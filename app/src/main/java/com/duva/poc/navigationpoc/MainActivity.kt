package com.duva.poc.navigationpoc

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.duva.poc.model.QuestionSequence
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        start_quiz_button.setOnClickListener {
            val intent = AppNavigator.getQuestionOneIntent(
                this,
                QuestionSequence(listOf(), 0)
            )

            startActivity(intent)
        }
    }
}
