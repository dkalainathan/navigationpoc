package com.duva.poc.navigationpoc

import android.content.Context
import android.content.Intent
import com.duva.poc.answer.navigation.AnswerIntents
import com.duva.poc.model.QuestionSequence
import com.duva.poc.model.UserAnswer
import com.duva.poc.navigation.Navigator
import com.duva.poc.question_one.navigation.QuestionOneIntents
import com.duva.poc.questionthree.navigation.QuestionThreeIntents
import com.duva.poc.questiontwo.navigation.QuestionTwoIntents
import com.duva.poc.score.navigation.ScoreIntents

object AppNavigator: Navigator {

    init {
        com.duva.poc.answer.navigation.NavigatorProvider.navigator = this
        com.duva.poc.question_one.navigation.NavigatorProvider.navigator = this
        com.duva.poc.questiontwo.navigation.NavigatorProvider.navigator = this
        com.duva.poc.questionthree.navigation.NavigatorProvider.navigator = this
        com.duva.poc.score.navigation.NavigatorProvider.navigator = this
    }

    override fun getHomeIntent(context: Context): Intent {
        return Intent(context, MainActivity::class.java).apply {
            addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
        }
    }

    override fun getQuestionOneIntent(
        context: Context,
        sequence: QuestionSequence
    ): Intent {
        return QuestionOneIntents.questionOneIntent(context, sequence)
    }

    override fun getQuestionTwoIntent(
        context: Context,
        sequence: QuestionSequence
    ): Intent {
        return QuestionTwoIntents.questionTwoIntent(context, sequence)
    }

    override fun getQuestionThreeIntent(
        context: Context,
        sequence: QuestionSequence
    ): Intent {
        return QuestionThreeIntents.getQuestionThreeIntent(context, sequence)
    }

    override fun getAnwserIntent(
        context: Context,
        answer: UserAnswer,
        sequence: QuestionSequence
    ): Intent {
        return AnswerIntents.anwserIntent(context, answer, sequence)
    }

    override fun getScoreIntent(
        context: Context,
        sequence: QuestionSequence
    ): Intent {
        return ScoreIntents.getScoreIntent(context, sequence)
    }
}