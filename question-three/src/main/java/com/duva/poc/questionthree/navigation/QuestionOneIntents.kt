package com.duva.poc.questionthree.navigation

import android.content.Context
import android.content.Intent
import com.duva.poc.model.QuestionSequence
import com.duva.poc.questionthree.Egypt

object QuestionThreeIntents {

    fun getQuestionThreeIntent(
        context: Context,
        sequence: QuestionSequence
    ): Intent {
        return Intent(context, Egypt::class.java).apply {
            putExtras(Egypt.getStartArguments(sequence))
        }
    }
}