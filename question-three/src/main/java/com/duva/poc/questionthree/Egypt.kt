package com.duva.poc.questionthree

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.duva.poc.model.QuestionSequence
import com.duva.poc.model.UserAnswer
import com.duva.poc.questionthree.navigation.NavigatorProvider
import kotlinx.android.synthetic.main.layout_question_three.*

class Egypt: AppCompatActivity() {

    lateinit var sequence: QuestionSequence

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_question_three)

        sequence = intent.getSerializableExtra(SEQUENCE_KEY) as QuestionSequence

        osiris.setOnClickListener { redirectBadAnswer() }
        horus.setOnClickListener { redirectBadAnswer() }
        anubis.setOnClickListener { redirectBadAnswer() }

        re.setOnClickListener { redirectGoodAnswer() }
    }

    private fun redirectBadAnswer() {
        val intent = NavigatorProvider.navigator.getAnwserIntent(
            context = this,
            answer = UserAnswer(false, re.text.toString()),
            sequence = sequence.copy(sequence.done + 3)
        )

        startActivity(intent)
    }

    private fun redirectGoodAnswer() {
        val intent = NavigatorProvider.navigator.getAnwserIntent(
            context = this,
            answer = UserAnswer(true, ""),
            sequence = sequence.copy(sequence.done + 3)
        )

        startActivity(intent)
    }

    companion object {
        private const val SEQUENCE_KEY = "Egypt.SEQUENCE_KEY"

        fun getStartArguments(sequence: QuestionSequence): Bundle {
            return Bundle().apply {
                putSerializable(SEQUENCE_KEY, sequence)
            }
        }
    }
}